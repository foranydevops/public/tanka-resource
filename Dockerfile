FROM alpine:latest as builder

RUN apk update \
    && apk add curl

RUN curl -Lo /usr/local/bin/tk https://github.com/grafana/tanka/releases/latest/download/tk-linux-amd64 \
    && chmod a+x /usr/local/bin/tk

RUN curl -fsSL -o helm.tar.gz https://get.helm.sh/helm-v3.15.0-rc.2-linux-amd64.tar.gz \
    && tar -C /usr/local -xzf helm.tar.gz

RUN curl -Lo /usr/local/bin/kubectl "https://dl.k8s.io/release/$(curl -L -s https://dl.k8s.io/release/stable.txt)/bin/linux/amd64/kubectl"

RUN curl -fsSL -o gum.tar.gz https://github.com/charmbracelet/gum/releases/download/v0.14.0/gum_0.14.0_Linux_x86_64.tar.gz \
    && tar -C /usr/local -xzf gum.tar.gz

RUN curl -s -Lo /usr/local/bin/jb https://github.com/jsonnet-bundler/jsonnet-bundler/releases/latest/download/jb-linux-amd64 \
    && chmod a+x /usr/local/bin/jb

FROM alpine:latest

COPY --from=builder /usr/local/bin/tk /usr/local/bin/tk
COPY --from=builder /usr/local/linux-amd64/helm /usr/local/bin/helm
COPY --from=builder /usr/local/gum_0.14.0_Linux_x86_64/gum /usr/local/bin/gum
COPY --from=builder /usr/local/bin/kubectl /usr/local/bin/kubectl
COPY --from=builder /usr/local/bin/jb /usr/local/bin/jb

RUN apk update && apk add --update bash jq coreutils && mkdir /opt/resource \
    && chmod +x /usr/local/bin/tk /usr/local/bin/helm /usr/local/bin/kubectl /usr/local/bin/gum /usr/local/bin/jb

COPY bin/ /opt/resource

RUN chmod a+x /opt/resource/in /opt/resource/out /opt/resource/check