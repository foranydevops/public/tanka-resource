#!/bin/bash


docker build ./ -t localhost/tanka-resource

docker run --rm -i -v "${PWD}:${PWD}" -w "${PWD}" localhost/tanka-resource /opt/resource/out . < ./test/data_out_kubeconfig.json