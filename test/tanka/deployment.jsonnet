{
    "kind": "Deployment",
    "apiVersion": "apps/v1",
    "metadata": {
        "name": "test-app",
        "creationTimestamp": null,
        "labels": {
            "app": "test-app"
        }
    },
    "spec": {
        "replicas": 2,
        "selector": {
            "matchLabels": {
                "app": "test-app"
            }
        },
        "template": {
            "metadata": {
                "creationTimestamp": null,
                "labels": {
                    "app": "test-app"
                }
            },
            "spec": {
                "containers": [
                    {
                        "name": "busybox",
                        "image": "busybox",
                        "ports": [
                            {
                                "containerPort": 3000
                            }
                        ],
                        "resources": {}
                    }
                ]
            }
        },
        "strategy": {}
    },
    "status": {}
}